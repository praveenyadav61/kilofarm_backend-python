from flask import Flask
from flask_cors import CORS, cross_origin
from flask import jsonify,request,make_response,session
from flask import flash
from flask_mysqldb import MySQL
import jwt
from datetime import datetime, timedelta
from functools import wraps
from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__)
app.config['MYSQL_HOST']='localhost'
app.config['MYSQL_USER']='root'
app.config['MYSQL_PASSWORD']='Pr@123456'
app.config['MYSQL_DB']='flaskapp'
app.config["SECRET_KEY"] = "supersecret"

CORS(app)
mysql = MySQL(app)
#auth = HTTPBasicAuth(app)

def check_for_token(func):
    @wraps(func)
    def wrapped(*args,**kwargs):
        token=request.args.get('token')
        if not token:
            return jsonify({'message':'Missing token'}),403
        try:
            data =jwt.decode(token,app.config['SECRET_KEY'])
        except:
            return jsonify({'message':'invalid token'}),403
        return func(*args,**kwargs)
    return wrapped

@app.route('/')
@check_for_token
def index():
	return 'hello'

@app.route('/login', methods=['GET'])
def login():
    userDetails=request.json
    username = userDetails['userName']
    password = userDetails['password']
    cursor=mysql.connection.cursor()
    cursor.execute("SELECT id, userName, password FROM users WHERE userName=%s", [username])
    row=cursor.fetchone()
    print(row)
    if row and (row[2]==password):
        
        session['logged_in']=True
        token=jwt.encode({'userName': row[1],'exp' :datetime.utcnow() + timedelta(minutes= 60)  },app.config['SECRET_KEY'])
        return jsonify({'token':token.decode('utf-8')})
        #return jsonify('You are successfully logged in....')
    else:
        return "invalid credentials"

@app.route('/signup',methods=["POST"])
def signup():
    userDetails=request.json
    username=userDetails['userName']
    phoneNumber=userDetails['phoneNumber']
    DOB=userDetails['DOB']
    password=userDetails['password']
    cur=mysql.connection.cursor()
    cur.execute("SELECT userName FROM users WHERE userName=%s", [username])
    check=cur.fetchall()
    if len(check)>0:
        return 'username already exists'
    cur.execute("INSERT INTO users(userName, phoneNumber, DOB, password) VALUES(%s, %s, %s, %s)",(username, phoneNumber,DOB,password))
    mysql.connection.commit()
    cur.close()
    return 'successful sign up'

@app.route('/createSKU', methods=['POST'])
@check_for_token
def create_SKU():
    try:
        cur=mysql.connection.cursor()
        json_obj=request.json
        name = json_obj['sku_name']
        category = json_obj['sku_category']
        price = json_obj['price']
        cur.execute("INSERT INTO `products`(`sku_name`,`sku_category`,`price`) VALUES (%s,%s,%s)",(name,category,price))
        mysql.connection.commit()
        response = jsonify('product added successfully!')
        response.status_code = 200
        return response
    except Exception as e:print(e)

    
@app.route('/get/<int:id>',methods=['GET'])
@check_for_token
def get(id):
    cur=mysql.connection.cursor()
    cur.execute("SELECT * FROM products WHERE id=%s",(id,))
    response=cur.fetchall()
    #print(res)
    if len(response)>0:return jsonify(response)
    else: return 'product id not valid'
@app.route('/update/<int:id>',methods=['PUT'])
@check_for_token
def update(id):
    cur=mysql.connection.cursor()
    json_obj=request.json
    name = json_obj['sku_name']
    category = json_obj['sku_category']
    price = json_obj['price']
    cur.execute("UPDATE `products` SET `sku_name`=%s,`sku_category`=%s,`price`=%s WHERE `id`=%s",(name,category,price,id))
    mysql.connection.commit()
    response = jsonify('product updated successfully!')
    return response


@app.route('/delete/<int:id>',methods=['DELETE'])
@check_for_token
def delete(id):
    cur=mysql.connection.cursor()
    cur.execute("DELETE FROM products WHERE id=%s",(id,))
    mysql.connection.commit()
    response=jsonify("product deleted successfully!")
    return response

    
if __name__ == "__main__":
    app.run(debug=True)