from flask import Flask
from flask_cors import CORS, cross_origin
from flask import jsonify,request
from flask import flash
app = Flask(__name__)
from flask_mysqldb import MySQL

app.config['MYSQL_HOST']='localhost'
app.config['MYSQL_USER']='root'
app.config['MYSQL_PASSWORD']='Pr@123456'
app.config['MYSQL_DB']='flaskapp'
app.config['CORS_HEADERS'] = 'Content-Type'
mysql = MySQL(app)
CORS(app)

@app.route('/')
def index():
    cur=mysql.connection.cursor()
    #cur.execute('''CREATE TABLE ex (id INTEGER NOT NULL, sku_name VARCHAR(20)) NOT NULL, sku_category VARCHAR(20)) NOT NULL, price INTEGER NOT NULL)''')
    #cur.execute('''CREATE TABLE ex(id INTEGER AUTO_INCREMENT,sku_name varchar(255) NOT NULL, sku_category varchar(255) NOT NULL, price INTEGER, PRIMARY KEY(id))''')
    #cur.execute('''INSERT INTO `ex`(`sku_name`,`sku_category`,`price`) VALUES ('abcd','type2',300)''')
    #mysql.connection.commit()
    return jsonify("WELCOME")


@app.route('/createSKU', methods=['POST'])
#@basic_auth.required
def create_SKU():
    cur=mysql.connection.cursor()
    json_obj=request.json
    name = json_obj['sku_name']
    category = json_obj['sku_category']
    price = json_obj['price']
    cur.execute("INSERT INTO `products`(`sku_name`,`sku_category`,`price`) VALUES (%s,%s,%s)",(name,category,price))
    mysql.connection.commit()
    response = jsonify('product added successfully!')
    response.status_code = 200
    return response

    
@app.route('/get/<int:id>',methods=['GET'])
def get(id):
    cur=mysql.connection.cursor()
    cur.execute("SELECT * FROM products WHERE id=%s",(id,))
    response=cur.fetchall()
    #print(res)
    if len(response)>0:return jsonify(response)
    else: return 'product id not valid'
    '''{"sku_name": "ghj","sku_category":"type3","price":500}
    '''
@app.route('/update/<int:id>',methods=['PUT'])
def update(id):
    cur=mysql.connection.cursor()
    json_obj=request.json
    name = json_obj['sku_name']
    category = json_obj['sku_category']
    price = json_obj['price']
    cur.execute("UPDATE `products` SET `sku_name`=%s,`sku_category`=%s,`price`=%s WHERE `id`=%s",(name,category,price,id))
    mysql.connection.commit()
    response = jsonify('product updated successfully!')
    return response


@app.route('/delete/<int:id>',methods=['DELETE'])
def delete(id):
    cur=mysql.connection.cursor()
    cur.execute("DELETE FROM products WHERE id=%s",(id,))
    mysql.connection.commit()
    response=jsonify("product deleted successfully!")
    return response

if __name__ == "__main__":
    app.run(debug=True)