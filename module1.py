from flask import Flask
from flask_cors import CORS, cross_origin
from flask import jsonify,request
from flask import flash
from flask_mysqldb import MySQL
from flask_jwt_extended import create_access_token
from flask_jwt_extended import JWTManager
app = Flask(__name__)
app.config['MYSQL_HOST']='localhost'
app.config['MYSQL_USER']='root'
app.config['MYSQL_PASSWORD']='Pr@123456'
app.config['MYSQL_DB']='flaskapp'
app.config["JWT_SECRET_KEY"] = "super-secret"
#app.config['MYSQL_CURSORCLASS']='DictCursor'

CORS(app)
mysql = MySQL(app)
jwt = JWTManager(app)



@app.route('/')
def hello():
	return "hello"

@app.route('/login', methods=['GET'])
def login():
    userDetails=request.json
    username = userDetails['userName']
    password = userDetails['password']
    cursor=mysql.connection.cursor()
    cursor.execute("SELECT id, userName, password FROM users WHERE userName=%s", [username])
    row=cursor.fetchone()
    print(row)
    if row and (row[2]==password):
        access_token = create_access_token(identity=username)
        return jsonify(access_token=access_token)
        #return jsonify('You are successfully logged in....')
    else:
        return "invalid credentials"

@app.route('/signup',methods=["POST"])
def signup():
    userDetails=request.json
    username=userDetails['userName']
    phoneNumber=userDetails['phoneNumber']
    DOB=userDetails['DOB']
    password=userDetails['password']
    cur=mysql.connection.cursor()
    cur.execute("SELECT userName FROM users WHERE userName=%s", [username])
    check=cur.fetchall()
    if len(check)>0:
        return 'username already exists'
    cur.execute("INSERT INTO users(userName, phoneNumber, DOB, password) VALUES(%s, %s, %s, %s)",(username, phoneNumber,DOB,password))
    mysql.connection.commit()
    cur.close()
    return 'successful sign up'
    
if __name__ == "__main__":
    app.run(debug=True)